package com.example.ghost.comp496_android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;

public class MainActivity extends AppCompatActivity {
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button  firstButton = (Button) findViewById(R.id.btnChangeImage1);
        Button secondButton = (Button) findViewById(R.id.btnChangeImage2);
        Button thirdButton = (Button) findViewById(R.id.btnChangeImage3);

        firstButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,firstScreen.class);
                startActivity(intent);
            }
        });

        secondButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,secondScreen.class);
                startActivity(intent);
            }
        });

        thirdButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,thirdScreen.class);
                startActivity(intent);
            }
        });
    }

}
